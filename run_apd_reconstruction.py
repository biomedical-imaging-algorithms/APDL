"""
script that take th csv files with encoding with the proposed atlas
and does the back reconstruction of each image. As sub-step it compute
the reconstruction error to evaluate he parameters and export visualisation

EXAMPLE:
>> python run_apd_reconstruction.py \
    --path_in /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APDL_real

>> python run_apd_reconstruction.py \
    --path_in /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APD_real

>> python run_apd_reconstruction.py \
    --path_in /datagrid/Medical/microscopy/drosophila/RESULTS/experiments_APD_real

>> python run_apd_reconstruction.py \
    --path_in /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APD_temp \
    --names_expt ExperimentALPE_mp_real_type_3_segm_reg_binary_gene_ssmall_20160509-155333 \
    --nb_jobs 1

Copyright (C) 2015-2016 Jiri Borovec <jiri.borovec@fel.cvut.cz>
"""

import os
import glob
import json
import logging
import time
import gc
import argparse
import multiprocessing as mproc
from functools import partial

import tqdm
import numpy as np
import pandas as pd
from PIL import Image
import matplotlib.pyplot as plt

import pattern_weights as ptn_weight
import dataset_utils as gen_data

VISUAL = True
NB_THREADS = int(mproc.cpu_count() * .9)
PATH_EXPERIMENTS = '/datagrid/Medical/microscopy/drosophila/RESULTS/' \
                   'experiments_APD_real'
# PATH_EXPERIMENTS = '/datagrid/Medical/microscopy/drosophila/TEMPORARY/' \
#                    'experiments_APD_real'
PATH_IMAGES_RGB = '/datagrid/Medical/microscopy/drosophila/TEMPORARY'

DICT_PARAMS = {
    'path_in': PATH_EXPERIMENTS,
    'names_expt': None,
    'nb_jobs': NB_THREADS,
}

NAME_CONFIG = 'config.json'
PREFIX_ATLAS = 'atlas_'
PREFIX_ENCODE = 'encoding_'
PREFIX_RECONST = 'reconstruct_'
CSV_RECONT_DIFF = 'reconstruction_diff.csv'
TXT_RECONT_DIFF = 'reconstruction_diff.txt'
POSIX_CSV_NEW = '_geneNew.csv'


def create_args_parser(params=DICT_PARAMS):
    """ create simple arg parser with default values (input, output, dataset)

    :param params: {str: ...}
    :return: object argparse<in, ...>
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-in', '--path_in', type=str, required=False,
                        default=params['path_in'],
                        help='path to the input image dataset')
    parser.add_argument('-expt', '--names_expt', type=str, required=False, nargs='+',
                        default=params['names_expt'],
                        help='name of the experiment')
    parser.add_argument('--nb_jobs', type=int, required=False,
                        default=params['nb_jobs'],
                        help='number of processes running in parallel')
    return parser


def parse_arg_params(parser):
    """ parse basic args and return as dictionary

    :param parser: argparse
    :return: {str: ...}
    """
    args = vars(parser.parse_args())
    args['path_in'] = os.path.abspath(os.path.expanduser(args['path_in']))
    assert os.path.exists(args['path_in']), '%s' % args['path_in']
    if args['names_expt'] is None:
        args['names_expt'] = sorted([os.path.basename(p) for p
                                    in glob.glob(os.path.join(args['path_in'], '*'))
                                    if os.path.isdir(p)])
    elif isinstance(args['names_expt'], str):
        args['names_expt'] = [args['names_expt']]  # make it as list with single
    args['names_expt'] = [n for n in args['names_expt']
                         if os.path.exists(os.path.join(args['path_in'], n))]
    return args


def export_fig_reconstruction(path_out, name, segm_orig, segm_reconst, img_atlas,
                              img_rgb=None):
    """ visualise reconstruction together with the original segmentation

    :param path_out: str
    :param name: str
    :param segm_orig: np.array<height, width>
    :param segm_reconst: np.array<height, width>
    :param img_atlas: np.array<height, width>
    """
    fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(20, 8))
    ax[0].set_title('original')
    if img_rgb is not None:
        ax[0].imshow(img_rgb, alpha=0.9)
    else:
        ax[0].imshow(1 - segm_orig, cmap='Greys', alpha=0.7)
    ax[0].imshow(img_atlas, alpha=0.1)
    ax[0].axis('off')
    ax[0].contour(img_atlas > 0, linewidth=2)
    ax[0].contour(img_atlas, levels=np.unique(img_atlas),
                  linewidth=2, cmap=plt.cm.jet)

    ax[1].set_title('reconstructed segmentation')
    lut = plt.cm.get_cmap('jet', segm_reconst.max())(range(segm_reconst.max() + 1))
    im = lut[segm_reconst]
    im[segm_reconst == 0, :] = (1., 1., 1., 1.)
    ax[1].imshow(im, alpha=0.8)
    ax[1].axes.get_xaxis().set_ticklabels([])
    ax[1].axes.get_yaxis().set_ticklabels([])
    # ax[1].axis('off')
    ax[1].contour(segm_orig, levels=np.unique(segm_orig), linewidth=2, colors='k')

    ax[2].set_title('selected vs not selected segm.')
    # segm_select = np.array(segm_reconst > 0, dtype=np.int)
    # segm_select[np.logical_and(img_atlas > 0, segm_reconst == 0)] = -1
    # segm_select[0, :2] = [-1, 1]
    # ax[2].imshow(segm_select, cmap=plt.cm.RdYlGn)
    im = np.ones(segm_reconst.shape + (4,))
    im[np.logical_and(img_atlas > 0, segm_reconst > 0), :] = (0, 1, 0, 1)
    im[np.logical_and(img_atlas > 0, segm_reconst == 0), :] = (1, 0, 0, 1)
    ax[2].imshow(im, alpha=0.7)
    ax[2].axes.get_xaxis().set_ticklabels([])
    ax[2].axes.get_yaxis().set_ticklabels([])
    # ax[2].axis('off')
    ax[2].contour(img_atlas, levels=np.unique(img_atlas), linewidth=1, colors='w')
    ax[2].contour(segm_orig, levels=np.unique(segm_orig), linewidth=3, colors='k')

    p_fig = os.path.join(path_out, name + '.png')
    fig.savefig(p_fig, bbox_inches='tight')
    plt.close(fig)


def compute_reconstruction(img_name, dict_params, path_out, im_atlas,
                           weights=None, b_visu=False):
    """ reconstruct the segmentation from atlas and particular weights
    and compute the reconstruction error

    :param path_out: str
    :param dict_params:
    :param im_atlas: np.array<height, width>
    :param weights: [<0,1>]
    :param img_name: str
    :param b_visu: bool
    :return: {str: float}
    """
    segm_orig = load_segmentation(dict_params, img_name)
    img_rgb = load_image_rgb(dict_params, img_name)
    if weights is None:
        # recompute encoding and then does the reconstruction
        weights = ptn_weight.weights_image_atlas_overlap_major(segm_orig, im_atlas)
    segm_rect = np.zeros_like(im_atlas)
    for i, w in enumerate(weights):
        lb = i + 1
        if w == 1:
            segm_rect[im_atlas == lb] = lb
    logging.debug('segm unique: %s', repr(np.unique(segm_rect)))
    if b_visu:
        try:
            export_fig_reconstruction(path_out, img_name, segm_orig, segm_rect,
                                      im_atlas)
        except:
            # logging.error(traceback.format_exc())
            logging.warning('drawing fail for "%s"...', img_name)
    segm_bin = (segm_rect >= 1)
    diff = np.sum(segm_orig != segm_bin) / float(np.prod(segm_orig.shape))
    return img_name, diff


def find_relevant_atlas(name_csv, list_names_atlas):
    """ find match of encode among all possible atlases

    :param name_csv: str
    :param list_names_atlas: [str]
    :return: str
    """
    list_atlas = [n.replace(PREFIX_ATLAS, '').replace('.png', '')
                  for n in list_names_atlas]
    name = name_csv.replace(PREFIX_ENCODE, '').replace('.csv', '')
    if name in list_atlas:
        idx = list_atlas.index(name)
        return list_names_atlas[idx]


def load_atlas_image(path_atlas):
    """ load the atlas as norm labels to be small natural ints

    :param path_atlas: str
    :return: np.array<height, width>
    """
    assert os.path.exists(path_atlas)
    img_atlas = Image.open(path_atlas)
    # norm image to have labels as [0, 1, 2, ...]
    uq_labels = sorted([lb for nb, lb in img_atlas.getcolors() if nb > 0])
    lut = np.zeros(max(uq_labels) + 1)
    for i, lb in enumerate(uq_labels):
        lut[lb] = i
    img_atlas = lut[np.array(img_atlas)]
    # subtract background (set it as -1)
    # img_atlas -= 1
    logging.debug('Atlas: %s with labels: %s', repr(img_atlas.shape),
                  repr(np.unique(img_atlas).tolist()))
    return img_atlas.astype(np.int)


def load_segmentation(dict_params, img_name):
    """ load the segmenattion with values {0, 1}

    :param dict_params: {str: values}
    :param img_name: str
    :return: np.array<height, width>
    """
    path_img = os.path.join(dict_params['path_in'], dict_params['dataset'],
                            img_name + '.png')
    img = np.array(Image.open(path_img))
    img /= img.max()
    return img


def load_image_rgb(dict_params, img_name):
    """ load the segmenattion with values {0, 1}

    :param dict_params: {str: values}
    :param img_name: str
    :return: np.array<height, width>
    """
    path_img = os.path.join(dict_params['path_rgb'], img_name + '.png')
    if img_name == 'mean':
        return None
    if os.path.exists(path_img):
        img = np.array(Image.open(path_img))
    else:
        logging.warning('particular RGB image not exists "%s"', path_img)
        img = None
    return img


def export_fig_atlas(img_atlas, path_out, name, max_label=None):
    """ export the atlas to given folder and specific name

    :param path_out: str
    :param name: str
    :param img_atlas: np.array<height, width>
    """
    if max_label is None:
        max_label = np.max(img_atlas)
    fig = plt.figure(figsize=(8, 6))
    ax = fig.gca().imshow(img_atlas, vmax=max_label, interpolation='nearest')
    boundaries = (np.array(range(max_label + 1)) - 0.5).tolist() + [max_label + 0.5]
    cbar = plt.colorbar(ax, boundaries=boundaries)
    cbar.set_ticks(range(max_label + 1))
    cbar.set_ticklabels(['ptn %i' % lb for lb in range(max_label + 1)])
    fig.savefig(os.path.join(path_out, name + '_rgb.png'), bbox_inches='tight')
    plt.close(fig)


def perform_reconstruction_mproc(dict_params, name_csv, name_atlas, df_encode, img_atlas,
                                 nb_jobs=NB_THREADS, b_visu=False):
    """ perform the reconstruction in multi process mode

    :param dict_params: {}
    :param name_csv: str
    :param df_encode: DF
    :param img_atlas: np.array<height, width>
    :param nb_jobs: int
    :param b_visu: bool
    :return: DF
    """
    export_fig_atlas(img_atlas, dict_params['path_exp'], name_atlas)
    path_out = os.path.join(dict_params['path_exp'],
                            name_csv.replace(PREFIX_ENCODE, PREFIX_RECONST))
    if b_visu:
        if not os.path.exists(path_out):
            os.mkdir(path_out)
        export_fig_atlas(img_atlas, path_out, 'atlas')
    list_patterns = [col for col in df_encode.columns if col.startswith('ptn')]
    logging.debug('list of pattern names: %s', repr(list_patterns))
    list_idxs = [int(col[3:]) for col in list_patterns]
    assert list_idxs == sorted(list_idxs)
    assert np.max(img_atlas) <= len(list_patterns)

    wrapper_reconstruction = partial(compute_reconstruction, dict_params=dict_params,
                                     path_out=path_out, im_atlas=img_atlas, b_visu=b_visu)
    results = []
    tqdm_bar = tqdm.tqdm(total=len(df_encode))

    if nb_jobs > 1:
        logging.debug('computing %i samples in %i threads', len(df_encode), nb_jobs)
        mproc_pool = mproc.Pool(nb_jobs)
        for res in mproc_pool.imap_unordered(wrapper_reconstruction, df_encode.index):
            results.append(res)
            tqdm_bar.update()
        mproc_pool.close()
        mproc_pool.join()
    else:
        for res in map(wrapper_reconstruction, df_encode.index):
            results.append(res)
            tqdm_bar.update()

    df_diff = pd.DataFrame(results, columns=['image', name_csv])
    df_diff.set_index('image', inplace=True)
    logging.debug(repr(df_diff.describe()))
    return df_diff


def load_config_json(path_expt):
    """ load configuration from a experiment folder and correct
    some old parameter names to be compatible between ald and new versions

    :param path_expt: str
    :return: {str: ...}
    """
    with open(os.path.join(path_expt, NAME_CONFIG), 'r') as fp:
        dict_params = json.load(fp)
    # in case it was moved somewhere else
    dict_params['path_exp'] = path_expt
    for n in ['out_path', 'exp_path']:
        if n in dict_params:
            dict_params.pop(n)
    dict_params['path_out'] = os.path.dirname(path_expt)
    if 'in_path' in dict_params:
        dict_params['path_in'] = dict_params.pop('in_path')
    if 'sub_dataset' in dict_params:
        dict_params['path_in'] = os.path.join(dict_params['path_in'],
                                              dict_params.pop('dataset'))
        dict_params['dataset'] = dict_params.pop('sub_dataset')
    with open(os.path.join(path_expt, NAME_CONFIG), 'w') as fp:
        json.dump(dict_params, fp)
    return dict_params


def recompute_encoding(config, atlas):
    """ load images and according given atlas recompute the encoding

    :param config: {str: ...}
    :param atlas: np.array<height, width>
    :return: DF
    """
    path_in = os.path.join(config.get('path_in'), config.get('dataset'))
    imgs, im_names = gen_data.dataset_load_images(path_in)
    weights = [ptn_weight.weights_image_atlas_overlap_major(img, atlas) for img in imgs]
    df = pd.DataFrame(data=np.array(weights), index=im_names)
    df.columns = ['ptn {}'.format(lb + 1) for lb in df.columns]
    df.index.name = 'image'
    gc.collect(), time.sleep(1)
    return df


def process_experiment(path_expt, nb_jobs=NB_THREADS, path_img_rgb=PATH_IMAGES_RGB):
    """ process complete folder with experiment

    :param path_expt: str
    """
    logging.info('Experiment folder: \n "%s"', path_expt)
    dict_params = load_config_json(path_expt)
    dir_im = os.path.basename(dict_params['path_in'])
    dict_params['path_rgb'] = os.path.join(path_img_rgb,
                                           dir_im.replace('_segm_reg_binary', '_RGB_reg'))
    atlas_names = [os.path.basename(p) for p
                   in glob.glob(os.path.join(path_expt, PREFIX_ATLAS + '*.png'))]
    list_csv = [p for p in glob.glob(os.path.join(path_expt, PREFIX_ENCODE + '*.csv'))
                if not p.endswith(POSIX_CSV_NEW)]
    df_diffs_all = pd.DataFrame()
    for i, path_csv in enumerate(list_csv):
        name_csv = os.path.basename(path_csv)
        name_atlas = find_relevant_atlas(name_csv, atlas_names)
        logging.info('# %i / %i for Atlas: "%s" -> Encoding: "%s"',
                     i + 1, len(list_csv), name_atlas, name_csv)
        if name_atlas is None:
            continue
        # load the atlas
        path_atlas = os.path.join(path_expt, name_atlas)
        img_atlas = load_atlas_image(path_atlas)
        df_encode = pd.DataFrame.from_csv(path_csv)
        df_diff = perform_reconstruction_mproc(dict_params, name_csv.replace('.csv', ''),
                                               os.path.splitext(name_atlas)[0],
                                               df_encode, img_atlas, nb_jobs, VISUAL)
        df_diffs_all = pd.concat([df_diffs_all, df_diff], axis=1)
    df_diffs_all.to_csv(os.path.join(path_expt, CSV_RECONT_DIFF))
    if len(df_diffs_all) > 0:
        df_res = df_diffs_all.describe().transpose()[
            ['count', 'mean', 'std', 'min', 'max']]
        logging.info(repr(df_res))
        with open(os.path.join(path_expt, TXT_RECONT_DIFF), 'w') as fp:
            fp.write(repr(df_res))
    else:
        logging.error('no result parsed!')


def main():
    """ process complete list of experiments """
    logging.basicConfig(level=logging.INFO)
    logging.info('running...')

    arg_params = parse_arg_params(create_args_parser())
    logging.info('PARAMS: \n%s', '\n'.join(['"{}": \n\t {}'.format(k, v)
                                            for k, v in arg_params.iteritems()]))
    list_expt = [os.path.join(arg_params['path_in'], n) for n in arg_params['names_expt']]
    assert len(list_expt) > 0, 'No experiments found!'

    # tqdm_bar = tqdm.tqdm(total=len(list_expt))
    for i, path_expt in enumerate(list_expt):
        logging.info('experiment %i / %i -> %s', i + 1, len(list_expt), path_expt)
        process_experiment(path_expt, arg_params['nb_jobs'])
        gc.collect(), time.sleep(1)
        # tqdm_bar.update(1)

    logging.info('DONE')


if __name__ == '__main__':
    main()
