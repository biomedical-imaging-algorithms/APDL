"""
tha base class for all Atomic Pattern Dictionary methods
such as the stat of the art and our newly developed

Example run:
>> nohup python experiments_sta.py > ~/Medical-temp/experiments_APD-sta/nohup.log &

Copyright (C) 2015-2016 Jiri Borovec <jiri.borovec@fel.cvut.cz>
"""

# to suppress all visu, has to be on the beginning
# import matplotlib
# matplotlib.use('Agg')

import os
import copy
import time
import types
import argparse
import logging
import copy_reg
import tqdm
import multiprocessing as mproc

import numpy as np
import pandas as pd
from sklearn import metrics

import dataset_utils as gen_data
import pattern_disctionary as ptn_dict
import src.own_utils.tool_experiments as tl_expt


def _reduce_method(m):
    """ REQURED FOR MPROC POOL
    ISSUE: cPickle.PicklingError:
      Can't pickle <type 'instancemethod'>: attribute lookup __builtin__.instancemethod failed
    http://stackoverflow.com/questions/25156768/cant-pickle-type-instancemethod-using-pythons-multiprocessing-pool-apply-a
    """
    if m.im_self is None:
        return getattr, (m.im_class, m.im_func.func_name)
    else:
        return getattr, (m.im_self, m.im_func.func_name)

copy_reg.pickle(types.MethodType, _reduce_method)

NB_THREADS = int(mproc.cpu_count() * .9)
PATH_DATA_SYNTH = '/datagrid/Medical/microscopy/drosophila/synthetic_data'
PATH_DATA_REAL = '/datagrid/Medical/microscopy/drosophila/TEMPORARY'
PATH_RESULTS = '/datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APD_temp'
DEFAULT_PARAMS = {
    'computer': os.uname(),
    'nb_samples': None,
    'tol': 1e-3,
    'init_tp': 'msc',  # msc. rnd
    'max_iter': 25,  # 250
    'gc_regul': 0.,
    'nb_labels': 2,
    'nb_runs': NB_THREADS,  # 500
    'gc_reinit': True,
    'ptn_split': False,
    'ptn_compact': True,
    'overlap_mj': True,
}

SYNTH_DATASET_NAME = 'atomicPatternDictionary_v0'
SYNTH_PATH_APD = os.path.join(PATH_DATA_SYNTH, SYNTH_DATASET_NAME)
SYNTH_SUB_DATASETS = ['datasetBinary_raw',
                      'datasetBinary_noise',
                      'datasetBinary_deform',
                      'datasetBinary_defNoise']
SYNTH_PARAMS = DEFAULT_PARAMS.copy()
SYNTH_PARAMS.update({
    'path_in': SYNTH_PATH_APD,
    'dataset': SYNTH_SUB_DATASETS,
    'path_out': PATH_RESULTS,
})
SYNTH_PTN_RANGE = {
    'atomicPatternDictionary_00': range(5),
    'atomicPatternDictionary_v0': range(3, 15, 1),
    'atomicPatternDictionary_v1': range(5, 20, 1),
    'atomicPatternDictionary_v2': range(10, 40, 2) + [23],
    'atomicPatternDictionary_v3': range(10, 40, 2),
    'atomicPatternDictionary3D_v0': range(2, 14, 1),
    'atomicPatternDictionary3D_v1': range(6, 30, 2),
}
# SYNTH_RESULTS_NAME = 'experiments_APD'

REAL_DATASET_NAME = '1000_images_improved_binary'
# REAL_SUB_DATASETS = ['binary-fix', 'binary-otsu', 'binary-adapt']
NB_PATTERNS_REAL = [5, 8, 10, 12, 14, 16, 18, 20, 25, 30, 40, 60]
REAL_SUB_DATASETS = [
    # 'gene',
    # 'gene_small',
    'gene_ssmall',
]
REAL_PARAMS = DEFAULT_PARAMS.copy()
REAL_PARAMS.update({
    'path_in': os.path.join(PATH_DATA_REAL, REAL_DATASET_NAME),
    'dataset': REAL_SUB_DATASETS,
    'path_out': PATH_RESULTS,
    'max_iter': 50,
    'nb_runs': 10})
# PATH_OUTPUT = os.path.join('..','..','results')


def create_args_parser(dict_params):
    """ create simple arg parser with default values (input, output, dataset)

    :param dict_params: {str: ...}
    :return: object argparse<in, out, ant, name>
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-in', '--path_in', type=str, required=True,
                        help='path to the input image dataset',
                        default=dict_params['path_in'])
    parser.add_argument('-out', '--path_out', type=str, required=True,
                        help='path to the output with experiment results',
                        default=dict_params['path_out'])
    parser.add_argument('-tp', '--type', type=str, required=False,
                        help='switch between real and synth. images',
                        default='synth', choices=['real', 'synth'])
    parser.add_argument('-n', '--name', type=str, required=False,
                        help='specific name', default=None)
    parser.add_argument('--dataset', type=str, required=False, nargs='+', default=None,
                        help='name of dataset to be used')
    parser.add_argument('--nb_jobs', type=int, required=False, default=NB_THREADS,
                        help='number of processes running in parallel')
    parser.add_argument('--method', type=str, required=False, nargs='+', default=None,
                        help='possible APD methods',
                        choices=['PCA', 'ICA', 'DL', 'NMF', 'APDL'])
    return parser


def parse_arg_params(parser):
    """ parse basic args and return as dictionary

    :param parser: argparse
    :return: {str: ...}
    """
    args = vars(parser.parse_args())
    # remove not filled parameters
    args = {k: args[k] for k in args if args[k] is not None}
    for n in (k for k in args if 'path' in k and args[k] is not None):
        args[n] = os.path.abspath(os.path.expanduser(args[n]))
        assert os.path.exists(args[n]), '%s' % args[n]
    return args


def parse_params(default_params):
    parser = create_args_parser(default_params)
    params = copy.deepcopy(default_params)
    arg_params = parse_arg_params(parser)
    params.update(arg_params)
    return params


class ExperimentAPD(tl_expt.Experiment):
    """
    main class for APD experiments State-of-the-Art and ALPE
    """

    def __init__(self, dict_params):
        """ initialise class and set the experiment parameters

        :param dict_params: {str: ...}
        """
        if not 'name' in dict_params:
            dict_params['name'] = '{}_{}_{}'.format(dict_params['type'],
                                                    os.path.basename(dict_params['path_in']),
                                                    dict_params['dataset'])
        if not os.path.exists(dict_params['path_out']):
            os.mkdir(dict_params['path_out'])
        super(ExperimentAPD, self).__init__(dict_params)
        self.df_stat = pd.DataFrame()
        self.path_stat = os.path.join(self.params.get('path_exp'), self.RESULTS_TXT)
        # self.params.export_as(self.path_stat)
        str_params = 'PARAMETERS: \n' + '\n'.join(['"{}": \t {}'.format(k, v)
                                                   for k, v in self.params.iteritems()])
        logging.info(str_params)
        with open(self.path_stat, 'w') as fp:
            fp.write(str_params)

    def _load_data_ground_truth(self):
        """ loading all GT suh as atlas and reconstructed images from GT encoding

        :param params: {str: ...}, parameter settings
        """
        self.gt_atlas = gen_data.dataset_compose_atlas(self.params.get('path_in'))
        gt_encoding = gen_data.dataset_load_weights(self.params.get('path_in'))
        self.gt_img_rct = ptn_dict.reconstruct_samples(self.gt_atlas, gt_encoding)

    def _load_data(self, gt=True):
        """ load all required data for APD and also ground-truth if required

        :param gt: bool
        """
        logging.info('loading required data')
        self._load_images()
        if gt:
            self._load_data_ground_truth()
            assert len(self.imgs) == len(self.gt_img_rct)
        logging.debug('loaded %i images', len(self.imgs))
        self.imgs = [im.astype(np.uint8, copy=False) for im in self.imgs]

    def _load_images(self):
        """ load image data """
        path_data = os.path.join(self.params.get('path_in'),
                                 self.params.get('dataset'))
        self.imgs, self._im_names = gen_data.dataset_load_images(path_data)

    def run(self, gt=True, iter_var='case', iter_vals=range(1)):
        """ the main_real procedure that load, perform and evaluete experiment

        :param gt: bool
        :param iter_var: str name of variable to be iterated in the experiment
        :param iter_vals: [] list of possible values for :param iter_var:
        """
        logging.info('perform the complete experiment')
        self.iter_var_name = iter_var
        self.iter_values = iter_vals
        super(ExperimentAPD, self).run(gt)

    def _perform(self):
        """ perform experiment as sequence of iterated configurations """
        self._perform_sequence()

    def _perform_sequence(self):
        """ iteratively change a single experiment parameter with the same data
        """
        logging.info('perform_sequence in single thread')
        self.l_stat = []
        tqdm_bar = tqdm.tqdm(total=len(self.iter_values))
        for v in self.iter_values:
            self.params[self.iter_var_name] = v
            logging.debug(' -> set iterable "%s" on %s', self.iter_var_name,
                         repr(self.params[self.iter_var_name]))
            t = time.time()
            stat = self._perform_once(v)
            stat['time'] = time.time() - t
            self.l_stat.append(stat)
            logging.info('partial results: %s', repr(stat))
            tqdm_bar.update(1)
            # just partial export
            self._evaluate()

    def _perform_once(self, v):
        """ perform single experiment

        :param v: value
        :return: {str: val}
        """
        stat = {self.iter_var_name: v}
        return stat

    def _export_atlas(self, posix=''):
        """ export estimated atlas

        :param atlas: np.arra<h, w>
        :param posix: str
        """
        assert hasattr(self, 'atlas')
        n_img = 'atlas{}'.format(posix)
        gen_data.export_image(self.params.get('path_exp'), self.atlas, n_img)

    def _export_coding(self, posix=''):
        """ export estimated atlas

        :param atlas: np.array<height, width>
        :param posix: str
        """
        assert hasattr(self, 'w_bins')
        n_csv = 'encoding{}.csv'.format(posix)
        path_csv = os.path.join(self.params.get('path_exp'), n_csv)
        if not hasattr(self, '_im_names'):
            self._im_names = [str(i) for i in range(self.w_bins.shape[0])]
        df = pd.DataFrame(data=self.w_bins, index=self._im_names[:len(self.w_bins)])
        df.columns = ['ptn {:02d}'.format(lb + 1) for lb in df.columns]
        df.index.name = 'image'
        df.to_csv(path_csv)

    def _compute_statistic_gt(self, imgs_rct=None):
        """ compute the statistic gor GT and estimated atlas and reconstructed images

        :param atlas: np.array<height, width>
        :param imgs_rct: [np.array<height, width>]
        :return: {str: float, }
        """
        stat = {}
        logging.debug('compute static - %s', hasattr(self, 'gt_atlas'))
        if hasattr(self, 'gt_atlas') and hasattr(self, 'atlas'):
            if self.gt_atlas.shape == self.atlas.shape:
                stat['atlas_ARS'] = metrics.adjusted_rand_score(self.gt_atlas.ravel(),
                                                                self.atlas.ravel())
        logging.debug('compute reconstruction - %s', hasattr(self, 'gt_img_rct'))
        # error estimation from original reconstruction
        if hasattr(self, 'gt_img_rct') and imgs_rct is not None:
            # imgs_rct = ptn_dict.reconstruct_samples(self.atlas, self.w_bins)
            # imgs_rct = self._binarize_img_reconstruction(imgs_rct)
            imgs_gt = self.gt_img_rct[:len(imgs_rct)]
            diff = np.asarray(imgs_gt) - np.asarray(imgs_rct)
            stat['reconstruct_diff'] = np.sum(abs(diff)) / float(np.prod(diff.shape))
        elif hasattr(self, 'imgs') and imgs_rct is not None:
            imgs = self.imgs[:len(imgs_rct)]
            diff = np.asarray(imgs) - np.asarray(imgs_rct)
            stat['reconstruct_diff'] = np.sum(abs(diff)) / float(np.prod(diff.shape))
        return stat

    def _evaluate(self):
        """ evaluate experiment with given GT """
        self.df_stat = pd.DataFrame()
        for stat in self.l_stat:
            self.df_stat = self.df_stat.append(stat, ignore_index=True)
        if self.iter_var_name in stat:
            self.df_stat.set_index(self.iter_var_name, inplace=True)
        path_csv = os.path.join(self.params.get('path_exp'), self.RESULTS_CSV)
        logging.debug('save results: "%s"', path_csv)
        self.df_stat.to_csv(path_csv)

    def _summarise(self):
        """ summarise and export experiment results """
        logging.info('summarise the experiment')
        if hasattr(self, 'df_stat') and not self.df_stat.empty:
            with open(self.path_stat, 'a') as fp:
                fp.write('\n' * 3 + 'RESULTS: \n' + '=' * 9)
                fp.write('\n{}'.format(self.df_stat.describe()))
            logging.debug('statistic: \n%s', repr(self.df_stat.describe()))


class ExperimentAPD_parallel(ExperimentAPD):
    """
    run the experiment in multiple threads
    """

    def __init__(self, dict_params, nb_jobs=NB_THREADS):
        """ initialise parameters and nb jobs in parallel

        :param dict_params: {str: ...}
        :param nb_jobs: int
        """
        super(ExperimentAPD_parallel, self).__init__(dict_params)
        self.nb_jobs = nb_jobs

    def _load_images(self):
        """ load image data """
        path_data = os.path.join(self.params.get('path_in'),
                                 self.params.get('dataset'))
        self.imgs, self._im_names = gen_data.dataset_load_images(path_data,
                                                         nb_jobs=self.nb_jobs)

    def _warp_perform_once(self, v):
        self.params[self.iter_var_name] = v
        logging.debug(' -> set iterable "%s" on %s', self.iter_var_name,
                     repr(self.params[self.iter_var_name]))
        t = time.time()
        # stat = super(ExperimentAPD_mp, self)._perform_once(v)
        stat = self._perform_once(v)
        stat['time'] = time.time() - t
        logging.info('partial results: %s', repr(stat))
        return stat

    # def _perform_once(self, v):
    #     """ perform single experiment
    #
    #     :param v: value
    #     :return: {str: val}
    #     """
    #     t = time.time()
    #     self.params[self.iter_var_name] = v
    #     stat = super(ExperimentAPD_mp, self)._perform_once(v)
    #     stat['time'] = time.time() - t
    #     return stat

    def _perform_sequence(self):
        """ perform sequence in multiprocessing pool """
        logging.debug('perform_sequence in %i threads for %i values',
                      self.nb_jobs, len(self.iter_values))
        # ISSUE with passing large date to processes so the images are saved
        # and loaded in particular process again
        # p_imgs = os.path.join(self.params.get('path_exp'), 'input_images.npz')
        # np.savez(open(p_imgs, 'w'), imgs=self.imgs)

        self.l_stat = []
        # tqdm_bar = tqdm.tqdm(total=len(self.iter_values))
        mproc_pool = mproc.Pool(self.nb_jobs)
        for stat in mproc_pool.map(self._warp_perform_once, self.iter_values):
            self.l_stat.append(stat)
            self._evaluate()
            # tqdm_bar.update(1)
        mproc_pool.close()
        mproc_pool.join()

        # remove temporary image file
        # os.remove(p_imgs)


def extend_list_params(list_params, name_param, list_options):
    """ extend the parameter list by all sub-datasets

    :param list_params: [{str: ...}]
    :param name_param: str
    :param list_options: list
    :return: [{str: ...}]
    """
    if not isinstance(list_options, list):
        list_options = [list_options]
    list_params_new = []
    for param in list_params:
        for v in list_options:
            param_new = param.copy()
            param_new.update({name_param: v})
            list_params_new.append(param_new)
    return list_params_new