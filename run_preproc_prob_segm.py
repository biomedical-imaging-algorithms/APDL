"""
run experiments with Atomic Learning Pattern Encoding

Copyright (C) 2015-2016 Jiri Borovec <jiri.borovec@fel.cvut.cz>
"""


import os
import gc
import time
import logging
import multiprocessing as mproc

# to suppres all visu, has to be on the beginning
import matplotlib
matplotlib.use('Agg')
import numpy as np
from skimage import filters, morphology, transform

import dataset_utils as gen_data

logger = logging.getLogger(__name__)

# PATH_DATA = '/b_jirka/b_jirka/TEMP/APD_real_data'
DEFAULT_PATH_DATA = '/datagrid/Medical/microscopy/drosophila/'

# REAL_DATASET_NAME = '1000_ims'
# IMAGE_PATTERN = '*_exp'
# REAL_DATASET_NAME = '1000_images_improved'
# IMAGE_PATTERN = '*_seg_de'

REAL_DATASET_NAME = 'type_1_segm_reg'
IMAGE_PATTERN = '*'

DEFAULT_PARAMS = {
    # 'computer': os.uname(),
    'path_in': os.path.join(DEFAULT_PATH_DATA, 'RESULTS', REAL_DATASET_NAME),
    'path_out': os.path.join(DEFAULT_PATH_DATA, 'TEMPORARY'),
    # 'binary': ['fix', 'otsu', 'adapt'],
    'binary': ['3cls'],
}
BINARY_POSIX = '_binary'
NB_THREADS = int(mproc.cpu_count() * 0.7)
IMAGE_BINARY_THRESHOLD = 0.9


def extend_images(imgs):
    """ the method requres images of equal dims
    so if there is different image extend them by zeros

    :param imgs: [np.array<w, h>]
    :return: [np.array<w, h>]
    """
    im_sizes = [im.shape for im in imgs]
    logger.info('different image sizes: {}'.format(set(im_sizes)))
    if len(set(im_sizes)) == 1:
        return imgs
    w_max = max([w for w, h in im_sizes])
    h_max = max([h for w, h in im_sizes])
    m_size = (w_max, h_max)
    logger.debug('max image size: {}'.format(m_size))
    for i, im in enumerate(imgs):
        if im.shape == m_size:
            continue
        logger.debug('.. extend img {} dims {} -> {}'.format(i, im.shape, m_size))
        im_new = np.zeros(m_size)
        im_new[:im.sahpe[0], :im.sahpe[1]] = im
        imgs[i] = im_new
    return imgs


def find_borders(im_mean, v_lim=0.):
    """ find from sided the rows and col where the image set is zero

    :param v_lim:
    :param im_mean:
    :return:
    """
    border = {}
    logger.info('crop value threshold: {}'.format(v_lim))
    for i in range(len(im_mean.shape)):
        vals = im_mean.mean(axis=i)
        vals[vals < v_lim] = 0
        idx = np.nonzero(vals)[0]
        if len(idx) > 0:
            border[i] = min(idx), max(idx)
        else:
            logger.warning('nothing to chose in axis: {}'.format(i))
            border[i] = 0, -1
    return border


def crop_images(imgs, v_lim=0.):
    """ try to cut out image rows and colums that are useless

    :param imgs: [np.array<w, h>]
    :return: [np.array<w, h>]
    """
    logger.info('perform image crop...')
    im_mean = np.array(imgs).mean(axis=0)
    border = find_borders(im_mean, v_lim)
    logger.info('image crop limits: {}'.format(border))
    imgs_crop = [None] * len(imgs)
    logger.info('crop all {} images'.format(len(imgs)))
    for i, im in enumerate(imgs):
        # the sum dim is perpendicular to cut dim
        imgs_crop[i] = im[border[1][0]:border[1][1], border[0][0]:border[0][1]]
    logger.debug('image crop finished')
    return imgs_crop


def threshold_image_fix(idx_img):
    """ treashold imahe with givem value in range (0, 1)

    :param img:
    :return:
    """
    idx, img = idx_img
    img_th = img > IMAGE_BINARY_THRESHOLD
    return idx, img_th


def threshold_image_otsu(idx_img):
    idx, img = idx_img
    th = filters.threshold_otsu(img[img > 0])
    logger.debug('threshold with val: {}'.format(th))
    img_th = img > th
    return idx, img_th


def threshold_image_adapt(idx_img):
    idx, img = idx_img
    img_th = filters.threshold_adaptive(img, 450)
    selem = morphology.disk(12)
    img_th = morphology.opening(img_th, selem)
    return idx, img_th


def threshold_image_3cls_gene(idx_img):
    idx, img = idx_img
    img_th = img >= (2 / 3.)
    return idx, img_th


def threshold_image_3cls_disc(idx_img):
    idx, img = idx_img
    img_th = img >= (1 / 3.)
    return idx, img_th


def scale_image(idx_img):
    idx, img = idx_img
    img_scale = transform.rescale(img, scale=0.5, order=0)
    return idx, img_scale


def apply_images(imgs, func, nb_jobs=NB_THREADS):
    """ threshold images by specific level

    :param nb_jobs:
    :param func:
    :param imgs: [np.array<w, h>]
    :return: [np.array<w, h>]
    """
    logger.info('apply function "%s on %i images...', func.__name__, len(imgs))
    if nb_jobs > 1:
        logger.info('running in %i threads...', nb_jobs)
        mproc_pool = mproc.Pool(nb_jobs)
        idx_imgs = mproc_pool.map(func, enumerate(imgs))
        mproc_pool.close()
        mproc_pool.join()
    else:
        logger.info('running single thread')
        idx_imgs = [func(im) for im in enumerate(imgs)]
    imgs_final = [img for _, img in sorted(idx_imgs)]
    return imgs_final


def binarize_all(params=DEFAULT_PARAMS, im_pattern=IMAGE_PATTERN, nb_jobs=NB_THREADS):
    """ run all experiments

    :return:
    """
    imgs, names = gen_data.dataset_load_images(params['path_in'], im_pattern=im_pattern,
                                               nb_jobs=nb_jobs)
    logger.info('loaded {} images of size {}'.format(len(imgs), imgs[0].shape))
    imgs = extend_images(imgs)
    imgs = crop_images(imgs, 1e-3)
    gc.collect(), time.sleep(1)

    path_export = os.path.join(params['path_out'],
                               os.path.basename(params['path_in']) + BINARY_POSIX)
    if not os.path.exists(path_export):
        os.mkdir(path_export)
    logger.debug('exporting path: %s', path_export)

    if 'fix' in params['binary']:
        imgs_th = apply_images(imgs, threshold_image_fix)
        name_dir = 'binary-fix_{}'.format(IMAGE_BINARY_THRESHOLD)
        gen_data.dataset_export_images(os.path.join(path_export, name_dir),
                                       imgs_th, names, nb_jobs=nb_jobs)
        gc.collect(), time.sleep(1)

    if 'otsu' in params['binary']:
        imgs_th = apply_images(imgs, threshold_image_otsu)
        gen_data.dataset_export_images(os.path.join(path_export, 'binary-otsu'),
                                       imgs_th, names, nb_jobs=nb_jobs)
        gc.collect(), time.sleep(1)

    if 'adapt' in params['binary']:
        imgs_th = apply_images(imgs, threshold_image_adapt)
        gen_data.dataset_export_images(os.path.join(path_export, 'binary-adapt'),
                                       imgs_th, names, nb_jobs=nb_jobs)
        gc.collect(), time.sleep(1)

    if '3cls' in params['binary']:
        logger.info('detecting just last class in 3-class segmentation')
        imgs_th = apply_images(imgs, threshold_image_3cls_gene)
        gen_data.dataset_export_images(os.path.join(path_export, 'gene'),
                                       imgs_th, names, nb_jobs=nb_jobs)
        gc.collect(), time.sleep(1)
        logger.info('scaling images by 0.5')
        imgs_scale = apply_images(imgs_th, scale_image)
        gen_data.dataset_export_images(os.path.join(path_export, 'gene_small'),
                                       imgs_scale, names, nb_jobs=nb_jobs)
        logger.info('scaling images by 0.5')
        imgs_scale = apply_images(imgs_scale, scale_image)
        gen_data.dataset_export_images(os.path.join(path_export, 'gene_ssmall'),
                                       imgs_scale, names, nb_jobs=nb_jobs)
        gc.collect(), time.sleep(1)

        logger.info('detecting foreground class in 3-class segmentation')
        imgs_th = apply_images(imgs, threshold_image_3cls_disc)
        gen_data.dataset_export_images(os.path.join(path_export, 'disc'),
                                       imgs_th, names, nb_jobs=nb_jobs)
        gc.collect(), time.sleep(1)


def main():
    logging.basicConfig(level=logging.DEBUG)
    logger.info('running...')

    params = DEFAULT_PARAMS.copy()
    datasets = ['type_{}_segm_reg'.format(i) for i in range(1, 5)]
    for ds in datasets:
        params['path_in'] = os.path.join(DEFAULT_PATH_DATA, 'RESULTS',
                                         REAL_DATASET_NAME, ds)
        binarize_all(params)

    logger.info('DONE')


if __name__ == "__main__":
    main()
