"""
run experiments tests

Copyright (C) 2015-2016 Jiri Borovec <jiri.borovec@fel.cvut.cz>
"""

import logging

import run_experiment_apd_all
import run_experiment_apd_apdl


def main():
    """ main_real entry point """
    logging.basicConfig(level=logging.INFO)
    logging.info('running...')

    run_experiment_apd_all.experiments_test()

    run_experiment_apd_apdl.experiments_test()

    logging.info('DONE')
    # plt.show()


if __name__ == '__main__':
    main()
