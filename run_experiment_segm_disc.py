"""
segmentation on disc images with specific configuration
* perform the segmentation and export results
* load results and does visualisation

Example run: (no params)

>> python run_experiment_segm_disc.py \
    -in ~/Dropbox/Workspace/py_ImageProcessing/images \
    -out ~/Dropbox/Workspace/py_ImageProcessing/output \
    --dataset drosophila_disc --nb_jobs 2

>> python run_experiment_segm_disc.py \
    -in /datagrid/Medical/microscopy/drosophila/disc_images_types \
    -out /datagrid/Medical/microscopy/drosophila/TEMPORARY/segm_disc_pproc-gc-edge-weight \
    --dataset type_3 --visual 1

DONE:
* visualise prob weights for each class
* visualise edge weights per sperpixel pair
https://vcansimplify.wordpress.com/2014/07/06/scikit-image-rag-introduction/
http://scipy8.rssing.com/chan-20823059/all_p20.html
https://vcansimplify.wordpress.com/category/python-2/

Copyright (C) 2015-2016 Jiri Borovec <jiri.borovec@fel.cvut.cz>
"""


import os
import sys
import glob
import time
import gc
import argparse
import logging
import traceback
import json
import multiprocessing as mproc
from functools import partial

# to suppress all visu, has to be on the beginning
# from paramiko.logging22 import logger

import tqdm
import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage
from skimage import io, exposure, restoration, measure, draw, morphology
from skimage import segmentation as ski_segm

sys.path.append(os.path.abspath(os.path.join('..','..')))
import src.segmentation.pipelines as segm
import src.own_utils.tool_experiments as tl_expt


DATASET = 'orig'
PATH_DATA = '/datagrid/Medical/microscopy/drosophila/disc_types'
# PATH_RESULTS = '/datagrid/Medical/microscopy/drosophila/RESULTS'
PATH_RESULTS = '/datagrid/Medical/microscopy/drosophila/TEMPORARY'
PREFIX_VISU_SEGM = 'visu_segm_'
PREFIX_VISU_NORM = 'visu_norm_'
PREFIX_VISU_PIPELINE = 'visu_'
IMAGE_NAME = '*.png'
NB_THREADS = int(mproc.cpu_count() * .8)

DEFAULT_PARAMS = {
    'computer': os.uname(),
    'sp_size': 20,
    'sp_regul': 0.15,
    'gc_regul': 0.9, # 1.2
    'gc_edge_type': 'weight', # 'const', 'weight'
    'nb_labels': 3,
    'fts': {'clr': ['mean', 'std', 'eng']},
    # 'fts': {'clr': ['median', 'std', 'eng']},
    'clr': 'rgb',  # 'lab'
    'proba_type': 'quantiles', # 'GMM', 'quantiles', 'kmeans'
    # 'visu': True,
}


def aparse_params():
    """
    SEE: https://docs.python.org/3/library/argparse.html
    :return: {str: str}, int
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-in', '--path_in', type=str, required=False,
                        help='path to the input directory', default=PATH_DATA)
    parser.add_argument('-out', '--path_out', type=str, required=False,
                        help='path to the output directory', default=PATH_RESULTS)
    parser.add_argument('--dataset', type=str, required=False,
                        help='name of the dataset to be segmented', default=DATASET)
    parser.add_argument('--nb_jobs', type=int, required=False, default=NB_THREADS,
                        help='number of processes in parallel')
    parser.add_argument('--visual', type=int, required=False, default=0,
                        help='whether di the visualisation')
    args = parser.parse_args()
    # args = vars(parser.parse_args())
    args.path_in = os.path.abspath(os.path.expanduser(args.path_in))
    assert os.path.exists(args.path_in), '%s' % args.path_in
    args.path_out = os.path.abspath(os.path.expanduser(args.path_out))
    assert os.path.exists(args.path_out), '%s' % args.path_out
    dict_paths = {
        'dataset_name': args.dataset,
        'path_in': os.path.join(args.path_in, args.dataset),
        'path_norm': os.path.join(args.path_out, args.dataset + '_norm'),
        'path_result': os.path.join(args.path_out, args.dataset + '_segm'),
        'path_visu': os.path.join(args.path_out, args.dataset + '_visu'),
    }
    assert os.path.exists(dict_paths['path_in']), '%s' % dict_paths['path_in']
    return dict_paths, args.nb_jobs, bool(args.visual)


def segments_sum_filled(seg):
    """ take each class and find smallest compact class

    :param seg: np.array<w, h>
    :return: [int]
    """
    # plt.figure()
    lb_sum = {}
    for i, lb in enumerate(np.unique(seg)):
        im = (seg == lb)
        im = morphology.binary_closing(im, morphology.disk(25))
        im = ndimage.binary_fill_holes(im)
        lb_sum[lb] = np.sum(im)
        # plt.subplot(1, 3, i+1), plt.imshow(im)
    lbs = sorted(lb_sum, key=lambda x: lb_sum[x], reverse=True)
    logging.debug('lb_sum: %s', repr(lb_sum))
    # plt.show()
    return lbs


def segment_values(img, seg):
    """ sort segments according mean image value

    :param img:
    :param seg:
    :return:
    """
    img_vec = img.reshape(-1, img.shape[-1])
    seg_vec = seg.reshape(-1)
    label_val = {}
    for i, lb in enumerate(np.unique(seg_vec)):
        im = img_vec[(seg_vec == lb)]
        label_val[lb] = np.median(im)
    labels = sorted(label_val, key=lambda x: label_val[x], reverse=True)
    logging.debug('lb_sum: %s', repr(label_val))
    return labels


def estimate_lut(img, seg):
    """ estimate relabeling LUT

    :param img:
    :param seg:
    :return:
    """
    # labels = segments_sum_filled(seg)
    labels = segment_values(img, seg)
    logging.debug('labels: %s & seg in range '
                 '%i:%i', repr(labels), np.min(seg), np.max(seg))
    lut = range(np.max(seg) + 1)
    for i, lb in enumerate(labels):
        lut[lb] = i
    return lut


def visual_pair_orig_segm(dict_paths, img, seg, img_name):
    """ visualise pair of original image and segm

    :param dict_paths:
    :param img:
    :param seg:
    :param img_name:
    """
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(24, 12))
    ax[0].set_title('original image')
    ax[0].imshow(img), ax[0].axis('off')
    ax[0].contour(seg, linewidth=2, cmap=plt.cm.Reds)
    ax[1].set_title('resulting segmentation')
    ax[1].imshow(seg), ax[1].axis('off')
    # ax[0].imshow(seg_sml), plt.axis('off')
    path_fig = os.path.join(dict_paths['path_visu'], PREFIX_VISU_SEGM + img_name)
    fig.savefig(path_fig, bbox_inches='tight')
    plt.close(fig)


def visual_pipeline(dict_paths, img, im_norm, seg_raw, seg, img_name):
    """ visualise complete pipeline process

    :param seg_raw:
    :param im_norm:
    :param dict_paths:
    :param img:
    :param seg:
    :param img_name:
    """
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(24, 20))
    ax[0, 0].set_title('original image')
    ax[0, 0].imshow(img), ax[0, 0].axis('off')
    ax[0, 0].contour(seg, linewidth=2, cmap=plt.cm.Reds)
    ax[1, 0].set_title('normalised image')
    ax[1, 0].imshow(im_norm), ax[1, 0].axis('off')
    ax[1, 0].contour(seg_raw, linewidth=2, cmap=plt.cm.Reds)
    ax[0, 1].set_title('resulting segmentation')
    ax[0, 1].imshow(seg), ax[0, 1].axis('off')
    ax[1, 1].set_title('raw segmentation')
    ax[1, 1].imshow(seg_raw), ax[1, 1].axis('off')
    # ax[0].imshow(seg_sml), plt.axis('off')
    path_fig = os.path.join(dict_paths['path_visu'], PREFIX_VISU_PIPELINE + img_name)
    fig.savefig(path_fig, bbox_inches='tight')
    plt.close(fig)


def draw_img_histogram(ax, img):
    ax.hist((img[:, :, 0], img[:, :, 1], img[:, :, 2]), bins=48,
            color=('red', 'green', 'blue'))
    # for i, clr in enumerate(list('rgb')):
    #     im_clr = img[:, :, i]
    #     count, bins = np.histogram(im_clr, bins=128)
    #     ax.plot(bins[:-1], count / float(np.product(im_clr.shape)))
    ax.set_xlabel('color values')
    ax.set_xlim([0, np.max(img)])
    ax.set_ylabel('[%]')
    ax.grid()


def visual_pair_orig_norm(dict_paths, img, img_norm, img_name):
    """ visualise pair of original image and segm

    :param dict_paths:
    :param img:
    :param img_norm:
    :param img_name:
    """
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(20, 15))
    ax[0, 0].set_title('original image')
    ax[0, 0].imshow(img), ax[0, 0].axis('off')
    draw_img_histogram(ax[1, 0], img)
    ax[0, 1].set_title('normalise image')
    ax[0, 1].imshow(img_norm), ax[0, 1].axis('off')
    draw_img_histogram(ax[1, 1], img_norm)
    path_fig = os.path.join(dict_paths['path_visu'], PREFIX_VISU_NORM +
                         img_name.replace('.png', '.pdf'))
    fig.savefig(path_fig, bbox_inches='tight')
    plt.close(fig)


def preprocessing_image(img):
    """ simple image pre-processing and nralisation

    :param img: np.array<h, w, 3>
    :return: np.array<h, w, 3>
    """
    logging.debug('perform the PRE-processing...')
    p_low = np.percentile(img, 2)
    p_high = np.percentile(img, 90)
    img_new = exposure.rescale_intensity(img, in_range=(p_low, p_high))
    # http://scikit-image.org/docs/dev/auto_examples/plot_denoise.html
    # PROBLEM with v0.13
    # img_new = restoration.denoise_bilateral(img_new, sigma_range=0.1,
    #                                         sigma_spatial=150)
    # http://scikit-image.org/docs/dev/auto_examples/plot_equalize.html
    # img_new = exposure.equalize_adapthist(img_new, clip_limit=0.05)
    return img_new


def postprocessing_segm_3cls(img, seg, label_disc=1, label_act=2):
    """ postprocessing and some morpho operations

    :param img: np.array<h, w, 3>
    :param seg: np.array<h, w>
    :return: np.array<h, w>
    """
    logging.info('perform the POST-processing...')
    # normalise and order labels
    seg -= seg.min()
    lut = estimate_lut(img, seg)
    logging.debug('LUT: {}'.format(lut))
    seg = np.asarray(lut)[seg]

    # DISCS
    seg_fg = seg >= label_disc
    seg_act = seg == label_act

    # correct the foreground layer
    seg_fg = morphology.binary_closing(seg_fg, morphology.disk(15))
    seg_fg = ndimage.binary_fill_holes(seg_fg)
    # # filter activation
    seg_act = morphology.binary_opening(seg_act, morphology.disk(9))

    # take centered elements
    sg, _ = ndimage.label(seg_fg)
    measure_seg = measure.regionprops(sg)
    lim_size = 0.1 * np.product(seg.shape)
    dist = [np.sum((0.5 * np.asarray(seg.shape) - np.asarray(m.centroid)) ** 2)
            for m in measure_seg if m.area > lim_size]
    if len(dist) == 0:
        return seg
    label_sel = measure_seg[dist.index(min(dist))].label
    seg_sel = (sg == label_sel)

    # TODO: active contours ??
    # seg_sel = segm_active_contours(img, seg_sel)

    seg_new = np.zeros_like(seg)
    seg_new[seg_sel] = label_disc
    seg_new[np.logical_and(seg_sel, seg_act)] = label_act
    return seg_new


def segm_active_contours(img, seg_fg):
    """
    http://scikit-image.org/docs/dev/api/skimage.segmentation.html#skimage.segmentation.active_contour

    :param img:
    :param seg_fg: np.array<h, w>
    :return: np.array<h, w>
    """
    logging.debug('perform active contours...')
    contours = measure.find_contours(seg_fg, 0)
    snake = ski_segm.active_contour(img, contours[0], max_iterations=999)
    mask = np.zeros_like(seg_fg)
    rr, cc = draw.polygon(snake[:, 0], snake[:, 1])
    mask[rr, cc] = 1
    return mask


def export_debug_images(path_out, img_name, dict_debug_imgs):
    """

    :param path_out:
    :param img_name:
    :param dict_debug_imgs:
    """
    path_debug = os.path.join(path_out, 'debug_' + os.path.splitext(img_name)[0])
    logging.debug('debug images: "%s"', repr(path_debug))
    if not os.path.exists(path_debug):
        os.mkdir(path_debug)
    for k in ['slic_mean', 'img_graph_edges', 'img_graph_segm']:
        # if k in dict_debug_imgs: # in case of GC const edges, there is no edge image
        io.imsave(os.path.join(path_debug, k + '.png'), dict_debug_imgs[k])
    img_contour = ski_segm.mark_boundaries(dict_debug_imgs['img'],
                                           dict_debug_imgs['slic'], (1, 0, 0))
    io.imsave(os.path.join(path_debug, 'slic_contour.png'), img_contour)
    for i, im_uc in enumerate(dict_debug_imgs['imgs_unary_cost']):
        io.imsave(os.path.join(path_debug, 'im_unary_cost_{}.png'.format(i)), im_uc)


def segment_image(path_img, params, dict_paths):
    """ segment individual image

    :param params: {str: ...}
    :param path_img: str
    :param dict_paths: {str: ...}
    :param visu: bool
    """
    img_name = os.path.basename(path_img)
    img_raw = io.imread(path_img)

    img = preprocessing_image(img_raw)
    io.imsave(os.path.join(dict_paths['path_norm'], img_name), img)

    logging.info('perform the SEGMENTATION...')
    logging.debug('image name: {}'.format(img_name))
    logging.debug('img values range: %f - %f', np.min(img), np.max(img))
    dict_debug_imgs = None
    if logging.getLogger().getEffectiveLevel() == logging.DEBUG:
        dict_debug_imgs = dict()

    t = time.time()
    seg_raw = segm.pipe_clr2d_spx_fts_gmm_gc(img, nb_cls=params['nb_labels'],
                                             clr_space=params['clr'],
                                             sp_size=params['sp_size'],
                                             sp_regul=params['sp_regul'],
                                             proba_type=params['proba_type'],
                                             gc_regul=params['gc_regul'],
                                             dict_features=params['fts'],
                                             gc_edge_type=params['gc_edge_type'],
                                             dict_debug_imgs=dict_debug_imgs)
    logging.info('execution time [s]: %f', (time.time()-t))

    if logging.getLogger().getEffectiveLevel() == logging.DEBUG:
        dict_debug_imgs['img'] = img_raw
        dict_debug_imgs['img_norm'] = img
        export_debug_images(dict_paths['path_result'], img_name, dict_debug_imgs)

    seg = postprocessing_segm_3cls(img, seg_raw)

    path_seg = os.path.join(dict_paths['path_result'], img_name)
    # io.imsave(p_img, np.array((seg == lbs[-1]) * 255, dtype=np.uint8))
    logging.debug('image save to "%s"', path_seg)
    io.imsave(path_seg, np.array(seg, dtype=np.uint8))
    visual_pipeline(dict_paths, img_raw, img, seg_raw, seg, img_name)
    gc.collect(), time.sleep(1)


def segment_image_folder(dict_paths, params=DEFAULT_PARAMS,
                         im_pattern=IMAGE_NAME, nb_jobs=1):
    """ segment complete image folder

    :param dict_paths: {str: ...}
    :param params: {str: ...}
    :param im_pattern: str
    """

    check_create_dirs(dict_paths, ['path_in'],
                      ['path_norm', 'path_result', 'path_visu'])
    with open(os.path.join(dict_paths['path_result'], 'config.txt'), 'w') as f:
        f.write(tl_expt.string_dict(params))
    json.dump(params, open(os.path.join(dict_paths['path_result'], 'config.json'), 'w'))
    paths_img = sorted(glob.glob(os.path.join(dict_paths['path_in'], im_pattern)))
    logging.info('found %i images', len(paths_img))

    tqdm_bar = tqdm.tqdm(total=len(paths_img))
    wrapper_segment_image = partial(segment_image,
                                    params=params, dict_paths=dict_paths)

    if nb_jobs > 1:
        logging.debug('wrapper_segment_image in %i threads', nb_jobs)
        mproc_pool = mproc.Pool(nb_jobs)
        for x in mproc_pool.imap_unordered(wrapper_segment_image, paths_img):
            tqdm_bar.update()
        mproc_pool.close()
        mproc_pool.join()
    else:
        for path_img in paths_img:
            segment_image(path_img, params, dict_paths)
            tqdm_bar.update()


def load_visual_pair_orig_segm(name_img, dict_paths):
    """ visualise segmentation together with original images, version fro mproc

    :param mp_set: (params, n_img)
    """
    logging.debug('visual: "%s"', name_img)
    path_img = os.path.join(dict_paths['path_in'], name_img)
    path_seg = os.path.join(dict_paths['path_result'], name_img)
    if not os.path.exists(path_img) or not os.path.exists(path_seg):
        logging.warning('image of segment. does not exist')
        return
    img = io.imread(path_img)
    seg = io.imread(path_seg)
    visual_pair_orig_segm(dict_paths, img, seg, name_img)


def check_create_dirs(dict_paths, l_required, l_create):
    """ check path existence and create some new

    :param dict_paths: {str: str}
    :param l_required: [str] required dirs
    :param l_create: [str] create new dirs
    """
    for n in l_create:
        if not os.path.exists(dict_paths[n]):
            os.mkdir(dict_paths[n])
    for n in l_required + l_create:
        logging.info('"%s" dir: (%s) <- %s', n, os.path.exists(dict_paths[n]), dict_paths[n])
    if any([not os.path.exists(dict_paths[n]) for n in l_required + l_create]):
        raise Exception('one or more paths do not exist')


def visu_folder_imgs_segm(dict_paths, im_pattern=IMAGE_NAME, nb_jobs=NB_THREADS):
    """ visualise complete folder

    :param params: {str: ...}
    :param im_pattern: str
    """
    check_create_dirs(dict_paths, ['path_in', 'path_result'], ['path_visu'])
    path_segs = glob.glob(os.path.join(dict_paths['path_result'], im_pattern))
    logging.info('found %i segmentation', len(path_segs))
    logging.debug('segmentation: %s...', repr(path_segs[:3]))

    tqdm_bar = tqdm.tqdm(total=len(path_segs))
    wrapper_visual = partial(load_visual_pair_orig_segm, dict_paths=dict_paths)

    mproc_pool = mproc.Pool(nb_jobs)
    for x in mproc_pool.imap_unordered(wrapper_visual,
                                       (os.path.basename(p) for p in path_segs)):
        tqdm_bar.update()
    mproc_pool.close()
    mproc_pool.join()


def norm_raw_image(name_img, dict_paths):
    """ load input raw image, normalise it and then save it

    :param name_img:
    :param dict_paths:
    """
    try:
        img = io.imread(os.path.join(dict_paths['path_in'], name_img))
        im_norm = preprocessing_image(img)
        # io.imsave(os.path.join(dict_paths['path_norm'], name_img), im_norm)
        visual_pair_orig_norm(dict_paths, img, im_norm, name_img)
    except:
        logging.error('image normal fail: "%s"', name_img)
        logging.error(traceback.format_exc())


def norm_folder_raw_imgs(dict_paths, nb_jobs=NB_THREADS, im_pattern=IMAGE_NAME):
    """ visualise complete folder

    :param dict_paths: {str: ...}
    :param im_pattern: str
    """
    check_create_dirs(dict_paths, ['path_in'], ['path_norm', 'path_visu'])
    path_imgs = glob.glob(os.path.join(dict_paths['path_in'], im_pattern))
    logging.info('found %i images', len(path_imgs))

    tqdm_bar = tqdm.tqdm(total=len(path_imgs))
    wrapper_norm_raw_image = partial(norm_raw_image, dict_paths=dict_paths)

    mproc_pool = mproc.Pool(nb_jobs)
    for x in mproc_pool.imap_unordered(wrapper_norm_raw_image,
                                       (os.path.basename(p) for p in path_imgs)):
        tqdm_bar.update()
    mproc_pool.close()
    mproc_pool.join()


# def main_segment_disc_types():
#     """ perform the segmentation pipeline with exporting normed images
#     and post visualisation over all types of discs """
#     for idx in range(1, 5):
#         n_dataset = 'type_{}'.format(idx)
#         logging.info('segmenting: %s', n_dataset)
#         d_paths = DEFAULT_PATHS.copy()
#         d_paths.update({
#             'dataset_name': n_dataset,
#             'path_in': os.path.join(PATH_DATA, n_dataset),
#             'path_norm': os.path.join(PATH_TEMP, n_dataset + '_norm'),
#             'path_result': os.path.join(PATH_RESULTS, n_dataset + '_segm'),
#             'path_visu': os.path.join(PATH_TEMP, n_dataset + '_visu'),})
#         norm_folder_raw_imgs(dict_paths=d_paths)
#         segment_image_folder(dict_paths=d_paths, nb_jobs=NB_THREADS)
#         visu_folder_imgs_segm(dict_paths=d_paths)


def main():
    logging.basicConfig(level=logging.INFO)
    logging.info('running...')

    dict_paths, nb_jobs, b_visu = aparse_params()
    if b_visu:
        logging.getLogger().setLevel(logging.DEBUG)

    segment_image_folder(dict_paths, nb_jobs=nb_jobs)

    # if b_visu:
    #     norm_folder_raw_imgs(dict_paths, nb_jobs)
    #     visu_folder_imgs_segm(dict_paths, nb_jobs=nb_jobs)

    logging.info('DONE')


if __name__ == "__main__":
    main()
