"""
Perform the hierarchical clustering on estimated atlas such as merging patterns
together with the smallest reconstruction error.
We assume going from all patters presented to single pattern in the atlas

EXAMPLE:
>> python run_apd_hierarchical_cluster.py \
    --path_in /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APDL_real

>> python run_apd_hierarchical_cluster.py \
    --path_in ~/Medical-data/microscopy/drosophila/RESULTS/experiments_APD_real

>> python run_apd_hierarchical_cluster.py \
    --path_in /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APD_temp \
    --names_expt ExperimentALPE_mp_real_type_3_segm_reg_binary_gene_ssmall_20160509-155333 \
    --nb_jobs 1

Copyright (C) 2015-2016 Jiri Borovec <jiri.borovec@fel.cvut.cz>
"""

import os
import glob
import logging
import gc
import sys
import time
import multiprocessing as mproc
from functools import partial

import tqdm
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import dataset_utils as gen_data
import run_apd_reconstruction as r_reconst
sys.path.append(os.path.abspath(os.path.join('..','..')))  # Add path to root
import src.segmentation.tool_superpixels as tl_spx

CONNECT_PATTERNS = True
NB_THREADS = int(mproc.cpu_count() * .9)
NAME_CONFIG = 'config.json'
PREFIX_ATLAS = 'atlas_'
PREFIX_ENCODE = 'encoding_'
PREFIX_RECONST = 'reconstruct_'
CSV_RECONT_DIFF = 'reconstruct_hierarchical_clustering.csv'
FIG_RECONT_DIFF = 'reconstruct_hierarchical_clustering.pdf'
POSIX_CSV_SKIP = r_reconst.POSIX_CSV_NEW
DIR_PREFIX = 'hierarchical_clustering_'
POSIX_MERGED = 'merged_nb_labels_%i'


def compute_merged_reconst_diff(ptn_comb, dict_params, path_out, atlas, img_names):
    """ merge pattern pair and compute reconstruction diff

    :param ptn_comb: (int, int)
    :param dict_params: {str: int}
    :param path_out: str
    :param atlas: np.array<height, width>
    :param img_names: [str]
    :return: (int, int), float
    """
    atlas_merge = atlas.copy()
    # merge pattern pair
    for lb in ptn_comb[1:]:
        atlas_merge[atlas_merge == lb] = ptn_comb[0]
    wrapper_reconstruction = partial(r_reconst.compute_reconstruction,
                                     dict_params=dict_params, path_out=path_out,
                                     im_atlas=atlas_merge)
    tuples_name_diff = map(wrapper_reconstruction, img_names)
    # compute mean diff over all reconst
    diff = np.mean(np.asarray(tuples_name_diff)[:, 1].astype(np.float))
    return ptn_comb, diff


def hierarchical_clustering_merge_patterns(dict_params, path_out, img_names,
                                           atlas, nb_jobs=NB_THREADS, connect_ptns=CONNECT_PATTERNS):
    """ using hierarchical clustering merge pattern pair and return partial results

    :param dict_params: {str: ...}
    :param path_out: str
    :param img_names: [str]
    :param atlas: np.array<height, width>
    :param nb_jobs: int
    :return: np.array<height, width>, (int, int), float
    """
    labels = sorted(np.unique(atlas).tolist())
    # generate combinations as list as skipping the 0 assuming on first position
    if connect_ptns:
        _, ptn_combines = tl_spx.make_graph_segm_connect2d_conn4(atlas)
    else:
        ptn_combines = [(labels[i], labels[j])
                        for i in range(1, len(labels)) for j in range(1, i)]
    assert len(ptn_combines) > 0 and \
           not any(len(set(ptn)) == 1 for ptn in ptn_combines)
    # parallel compute reconstructions
    wrapper_compute_merged = partial(compute_merged_reconst_diff,
                                     dict_params=dict_params, path_out=path_out,
                                     atlas=atlas, img_names=img_names)
    if nb_jobs > 1:
        mproc_pool = mproc.Pool(nb_jobs)
        tuples_ptn_diff = mproc_pool.map(wrapper_compute_merged, ptn_combines)
        mproc_pool.close()
        mproc_pool.join()
    else:
        tuples_ptn_diff = map(wrapper_compute_merged, ptn_combines)
    logging.debug('computed merged diffs: %s', repr(tuples_ptn_diff))
    idx_min = np.argmin(tuples_ptn_diff, axis=0)[1]
    ptn_comb, diff = tuples_ptn_diff[idx_min]
    logging.debug('found minimal pn pos %i for diff %f and patterns %s',
                  idx_min, diff, repr(ptn_comb))
    atlas_merged = atlas.copy()
    for lb in ptn_comb[1:]:
        atlas_merged[atlas_merged == lb] = ptn_comb[0]
    return atlas_merged, ptn_comb, diff


def export_partial_atlas_encode(dict_params, path_out, df_merged, max_label,
                                nb, atlas, ptn_comb, diff):
    """ export partial results such as atlas, encoding and reconstruct diff

    :param dict_params: {str: ...}
    :param path_out: str
    :param df_merged: DF
    :param max_label: int
    :param nb: int
    :param atlas: np.array<height, width>
    :param ptn_comb: (int, int)
    :param diff: float
    :return: DF
    """
    gen_data.export_image(path_out, atlas, PREFIX_ATLAS + POSIX_MERGED % nb)
    r_reconst.export_fig_atlas(atlas, path_out,
                               PREFIX_ATLAS + POSIX_MERGED % nb, max_label)
    df_encode = r_reconst.recompute_encoding(dict_params, atlas)
    df_encode.to_csv(os.path.join(path_out, PREFIX_ENCODE + POSIX_MERGED % nb + '.csv'))
    df_merged = df_merged.append({
        'nb_labels': nb,
        'merged': ptn_comb,
        'reconst_diff': diff}, ignore_index=True)
    return df_merged


def sequence_hierarchical_clustering(dict_params, path_out, img_names, atlas,
                                     nb_jobs=NB_THREADS):
    """ sequance if hierarchical clustering which decrease number of patterns
    by partial merging pattern pairs and exporting partial results

    :param dict_params: {str, ...}
    :param path_out: str
    :param img_names: [str]
    :param atlas: np.array<height, width>
    :param nb_jobs:
    :return: DF
    """
    if not os.path.exists(path_out):
        os.mkdir(path_out)
    nb_labels = len(np.unique(atlas))
    max_label = atlas.max()
    df_merged = pd.DataFrame()
    ptn_comb, diff = compute_merged_reconst_diff((0, 0), dict_params, path_out,
                                                 atlas, img_names)
    df_merged = export_partial_atlas_encode(dict_params, path_out, df_merged,
                                            max_label, nb_labels, atlas, ptn_comb, diff)
    # recursively merge patterns
    for nb in reversed(range(2, nb_labels)):
        atlas, ptn_comb, diff = hierarchical_clustering_merge_patterns(
                            dict_params, path_out, img_names, atlas, nb_jobs)
        df_merged = export_partial_atlas_encode(dict_params, path_out, df_merged,
                                                max_label, nb, atlas, ptn_comb, diff)
        df_merged.to_csv(os.path.join(path_out, CSV_RECONT_DIFF))
    df_merged.set_index('nb_labels', inplace=True)
    df_merged.to_csv(os.path.join(path_out, CSV_RECONT_DIFF))
    return df_merged


def plot_reconst_diff(path_csv):
    """ plot the reconst. diff from PD and expoert to a figure

    :param path_csv: str
    """
    assert os.path.exists(path_csv)
    df_diff = pd.DataFrame.from_csv(path_csv)
    fig = plt.figure()
    df_diff.plot(ax=fig.gca(), title='Reconstruction diff. for estimated atlases')
    fig.gca().set_ylabel('reconst. diff [%]')
    fig.gca().grid()
    path_fig = os.path.join(os.path.dirname(path_csv), FIG_RECONT_DIFF)
    fig.savefig(path_fig)


def process_experiment(path_expt, nb_jobs=NB_THREADS):
    """ process complete folder with experiment

    :param nb_jobs: int
    :param path_expt: str
    """
    logging.info('Experiment folder: \n "%s"', path_expt)
    dict_params = r_reconst.load_config_json(path_expt)
    atlas_names = [os.path.basename(p) for p
                   in glob.glob(os.path.join(path_expt, PREFIX_ATLAS + '*.png'))]
    list_csv = [p for p in glob.glob(os.path.join(path_expt, PREFIX_ENCODE + '*.csv'))
                if not p.endswith(POSIX_CSV_SKIP)]
    logging.debug('found %i CSV files: %s', len(list_csv), repr(list_csv))
    df_diffs_all = pd.DataFrame()
    for path_csv in sorted(list_csv):
        name_csv = os.path.basename(path_csv)
        name_atlas = r_reconst.find_relevant_atlas(name_csv, atlas_names)
        if name_atlas is None:
            logging.warning('nor related atlas for particular csv encoding "%s"',
                            name_csv)
            continue
        logging.info('Atlas: "%s" -> Encoding: "%s"', name_atlas, name_csv)
        path_atlas = os.path.join(path_expt, name_atlas)
        atlas = r_reconst.load_atlas_image(path_atlas)
        img_names = pd.DataFrame.from_csv(path_csv).index.tolist()
        path_out = os.path.join(path_expt, DIR_PREFIX + os.path.splitext(name_atlas)[0])
        df_diff = sequence_hierarchical_clustering(dict_params, path_out,
                                                   img_names, atlas, nb_jobs)
        # separet jut the recont dif and name it after atlas
        df_diff = df_diff['reconst_diff']
        df_diff.name = os.path.splitext(name_atlas)[0]
        logging.debug('records: %i for "%s"', len(df_diff), df_diff.name)
        df_diffs_all = pd.concat([df_diffs_all, df_diff], axis=1)
        df_diffs_all.to_csv(os.path.join(path_expt, CSV_RECONT_DIFF))
    logging.info('processed files: %s', repr(df_diffs_all.columns))


def main():
    """ process complete list of experiments """
    logging.basicConfig(level=logging.INFO)
    logging.info('running...')

    arg_params = r_reconst.parse_arg_params(r_reconst.create_args_parser())
    logging.info('PARAMS: \n%s', '\n'.join(['"{}": \n\t {}'.format(k, v)
                                            for k, v in arg_params.iteritems()]))
    list_expt = [os.path.join(arg_params['path_in'], n) for n in arg_params['names_expt']]
    assert len(list_expt) > 0, 'No experiments found!'

    tqdm_bar = tqdm.tqdm(total=len(list_expt))
    for path_expt in list_expt:
        process_experiment(path_expt, arg_params['nb_jobs'])
        plot_reconst_diff(os.path.join(path_expt, CSV_RECONT_DIFF))
        gc.collect(), time.sleep(1)
        tqdm_bar.update(1)

    logging.info('DONE')


if __name__ == '__main__':
    main()
