"""
script that walk over all segmentation, compute some statistic
and then decide while the segmentation is likely to be correct of not


Copyright (C) 2015-2016 Jiri Borovec <jiri.borovec@fel.cvut.cz>
"""

import os
import glob
import logging
import itertools
import shutil
import multiprocessing as mproc

# to suppress all visu, has to be on the beginning
import matplotlib
matplotlib.use('Agg')
import numpy as np
import pandas as pd
from skimage import io, morphology
import matplotlib.pyplot as plt

logger = logging.getLogger(__name__)

PATH_BASE = '/datagrid/Medical/microscopy/drosophila/'
PATH_SEGM = os.path.join(PATH_BASE, 'RESULTS/orig_segm')
PATH_VISU = os.path.join(PATH_BASE, 'TEMPORARY/orig_visu')
# PATH_SEGM = os.path.join(PATH_BASE, 'real_segmentations/stage_4_segm')
# PATH_VISU = os.path.join(PATH_BASE, 'real_segmentations/stage_4_visu')
NB_JOBS = mproc.cpu_count()
CSV_SEGM_GOOD = 'segm_good.csv'
CSV_SEGM_FAIL = 'segm_fail.csv'
FIG_STAT = 'stat_segm_labels.jpeg'
PREFIX_VISU_SEGM = 'visu_segm_'
# size around image borders
NB_IMG_CORNER = 50
# ration how much backround has to be around borders
THRESHOLD_CORNER_BG = 0.95
# ration for total num,ber of backround
THRESHOLD_BACKGROUND = 0.95
# ration of bacground in object convex hull
THRESHOLD_CONVEX = 0.85


def labels_ration(path_seg):
    """ compute radion among labels in segmentation as histogram

    :param path_seg: str
    :return: {int: float}
    """
    seg = io.imread(path_seg)
    n_seg = os.path.basename(path_seg)
    d_lb_hist = {lb: np.sum(seg == lb) / float(np.product(seg.shape) )
              for lb in np.unique(seg)}
    # the image borders
    seg_border = np.concatenate((seg[:NB_IMG_CORNER, :].ravel(),
                           seg[-NB_IMG_CORNER:, :].ravel(),
                           seg[:, :NB_IMG_CORNER].ravel(),
                           seg[:, -NB_IMG_CORNER:].ravel()))
    r_bg = np.sum(seg_border == 0) / float(seg_border.shape[0])
    seg_fg = morphology.binary_closing(seg > 0, morphology.disk(30))
    obj_convex = morphology.convex_hull_object(seg_fg)
    obj_bg = np.sum(seg_fg[obj_convex] > 0) / float(np.sum(obj_convex))
    return {'name': n_seg,
            'lb_hist': d_lb_hist,
            'r_bg': r_bg,
            'r_cx': obj_bg}


def plot_histo_labels(dict_hist, path_dir=''):
    """ plot some simple histogram

    :param path_dir: str
    :param dict_hist: {int: float}
    """
    logger.info('plotting stat. results')
    fig = plt.figure()
    for lb in dict_hist:
        plt.plot(dict_hist[lb], '+', label=str(lb))
    plt.xlim([0, max(len(v) for v in dict_hist.itervalues())])
    plt.xlabel('image samples')
    plt.ylabel('label cover')
    plt.legend(loc=0)
    plt.grid()
    if os.path.exists(path_dir):
        fig.savefig(os.path.join(path_dir, FIG_STAT))


def read_make_hist(paths_seg):
    """ in parallel read all segmentation and compute individual histogram

    :param paths_seg: [str], paths to all segmentation
    :return:[str, {int: float}] list or pairs with image name
            and relative label histogram
    """
    logger.debug('run in %i threads...', NB_JOBS)
    mproc_pool = mproc.Pool(NB_JOBS)
    l_desc = mproc_pool.map(labels_ration, paths_seg)
    mproc_pool.close()
    mproc_pool.join()
    return l_desc


def merge_hist_stat(list_name_hist):
    """ merge particular histograms per segmentation into one global per label

    :param list_name_hist: [str, {int: float}] list or pairs with image name
            and relative label histogram
    :return: {int: [float]}, histogram per label over all images
    """
    logger.debug('merge partial results...')
    l_hist = [l['lb_hist'] for l in list_name_hist]
    lbs = itertools.chain(*[h.keys() for h in l_hist])
    uq_lbs = np.unique(list(lbs)).tolist()
    dict_hist = {lb: [h[lb] for h in l_hist if lb in h]
                  for lb in uq_lbs}
    # join the foregrounds
    dict_hist['fg'] = []
    for hist in l_hist:
        dict_hist['fg'].append(hist.get(1, 0) + hist.get(2, 0))
    logger.debug('compute statistic...')
    for lb, vals in dict_hist.iteritems():
        if len(vals) == 0:
            logger.warning('label %s has no values to compute', str(lb))
            continue
        logger.info('label %s with mean %f, median %f, std %f',
                    str(lb), np.mean(vals), np.median(vals), np.std(vals))
        logger.debug(' -> count outliers: %i',
                     np.sum(abs(vals - np.median(vals)) > 3 * np.std(vals)))
    return dict_hist


def segm_decision(l_desc, dict_hist):
    """ according given rules decide weather the segmentation is good or not

    :param l_desc: [{str, {int: float}}] list or pairs with image name
            and relative label histogram
    :param dict_hist: {int: [float]}, histogram per label over all images
    :return: [str], [str]
    """
    l_good, l_fail = [], []
    fg_median, fg_std = np.median(dict_hist['fg']), np.std(dict_hist['fg'])
    for i, desc in enumerate(l_desc):
        fg = desc['lb_hist'].get(1, 0) + desc['lb_hist'].get(2, 0)
        b_range = abs(fg - fg_median) <= 3 * fg_std
        if b_range \
                and desc['lb_hist'][0] < THRESHOLD_BACKGROUND \
                and desc['r_bg'] > THRESHOLD_CORNER_BG \
                and desc['r_cx'] > THRESHOLD_CONVEX:
            l_good.append(desc['name'])
        else:
            l_fail.append(desc['name'])
    return l_good, l_fail


def export_results(path_dir, l_good, l_fail):
    """ export the results into csv file

    :param path_dir: str
    :param l_good: [str], names of images
    :param l_fail: [str], names of images
    """
    logger.info('export results as CSV files')
    pd.DataFrame(['images'] + l_good).to_csv(
        os.path.join(path_dir, CSV_SEGM_GOOD), index=False, header=False)
    pd.DataFrame(['images'] + l_fail).to_csv(
        os.path.join(path_dir, CSV_SEGM_FAIL), index=False, header=False)


def segm_detect_fails(path_dir=PATH_SEGM, im_pattern='*.png'):
    """ make the statistic over all segmentation in given folder
    and decide weather that are correct or fails

    :param path_dir: str
    :param im_pattern: str, pattern fro images
    """
    logger.info('FOLDER: "%s"', path_dir)
    if not os.path.exists(path_dir):
        raise Exception('folder "{}" dnot exist'.format(path_dir))
    p_segs = glob.glob(os.path.join(path_dir, im_pattern))
    logger.debug('found %i segmentation', len(p_segs))

    l_desc = read_make_hist(p_segs)

    d_hist = merge_hist_stat(l_desc)

    # make the decision while segm is fine
    l_good, l_fail = segm_decision(l_desc, d_hist)
    logger.info('number good %i and fails %i', len(l_good), len(l_fail))

    export_results(path_dir, l_good, l_fail)
    # show all samples
    plot_histo_labels(d_hist, path_dir)


def mproc_copy_file(mp_set):
    shutil.copyfile(*mp_set)


def copy_files(l_imgs, path_dir_visu, path_out):
    """ copy list of images in multi thread

    :param l_imgs: [str]
    :param path_dir_visu: str
    :param path_out: str
    """
    pp_dir_visu = os.path.join(path_dir_visu, PREFIX_VISU_SEGM)
    pp_out = os.path.join(path_out, PREFIX_VISU_SEGM)
    mp_set = [(pp_dir_visu + n_img, pp_out + n_img) for n_img in l_imgs]

    mproc_pool = mproc.Pool(NB_JOBS)
    mproc_pool.map(mproc_copy_file, mp_set)
    mproc_pool.close()
    mproc_pool.join()


def filter_copy_visu(path_dir_seg=PATH_SEGM, path_dir_visu=PATH_VISU):
    """ load csv file vith good and bad segmentation and in the visual folder
    create subfolder for good and bad segm and copy relevant iimages there

    :param path_dir_seg: str
    :param path_dir_visu: str
    """
    logger.info('filter and copy cases')
    logger.debug('segmentation: %s,\n visual: %s', path_dir_seg, path_dir_visu)
    for n_csv in [CSV_SEGM_GOOD, CSV_SEGM_FAIL]:
        logger.info('reading "%s"', n_csv)
        p_out = os.path.join(path_dir_visu, os.path.splitext(n_csv)[0])
        if os.path.exists(p_out):
            logger.debug('remove old dir %s', p_out)
            shutil.rmtree(p_out)
        os.mkdir(p_out)
        df = pd.DataFrame.from_csv(os.path.join(path_dir_seg, n_csv),
                                   index_col=False)
        logger.info('copy %i images to "%s"', len(df), n_csv)
        copy_files(df['images'].values.tolist(), path_dir_visu, p_out)


def main():
    """ the main_real entry point """
    logging.basicConfig(level=logging.DEBUG)
    logger.info('running...')
    # defaults run
    segm_detect_fails()
    filter_copy_visu()

    for idx in range(1, 5):
        p_dir_seg = os.path.join(PATH_BASE, 'RESULTS/type_{}_segm'.format(idx))
        p_dir_visu = os.path.join(PATH_BASE, 'TEMPORARY/type_{}_visu'.format(idx))
        segm_detect_fails(p_dir_seg)
        filter_copy_visu(p_dir_seg, p_dir_visu)

    logger.info('DONE')
    # plt.show()


if __name__ == '__main__':
    main()
