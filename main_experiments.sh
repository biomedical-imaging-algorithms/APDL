#!/bin/bash

#cd ~/Dropbox/Workspace/py_ImageProcessing/src/atm_ptn_dict/
#source ~/vEnv/bin/activate

# OUR method

python run_experiment_apd_apdl.py \
    -in /datagrid/Medical/microscopy/drosophila/synthetic_data/atomicPatternDictionary_v0 \
    -out /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APDL_synth

python run_experiment_apd_apdl.py \
    -in /datagrid/Medical/microscopy/drosophila/synthetic_data/atomicPatternDictionary_v1 \
    -out /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APDL_synth

python run_experiment_apd_apdl.py \
    -in /datagrid/Medical/microscopy/drosophila/synthetic_data/atomicPatternDictionary_v2 \
    -out /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APDL_synth

python run_experiment_apd_apdl.py \
    -in /datagrid/Medical/microscopy/drosophila/synthetic_data/atomicPatternDictionary_v3 \
    -out /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APDL_synth

# STATE-OF-THE-ART methods - SPCA, ICA, DL, NMF, APDL

  python run_experiment_apd_all.py \
     -in /datagrid/Medical/microscopy/drosophila/synthetic_data/atomicPatternDictionary_v0 \
     -out /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APD_synth

 python run_experiment_apd_all.py \
     -in /datagrid/Medical/microscopy/drosophila/synthetic_data/atomicPatternDictionary_v1 \
     -out /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APD_synth

 python run_experiment_apd_all.py \
     -in /datagrid/Medical/microscopy/drosophila/synthetic_data/atomicPatternDictionary_v2 \
     -out /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APD_synth

 python run_experiment_apd_all.py \
     -in /datagrid/Medical/microscopy/drosophila/synthetic_data/atomicPatternDictionary_v3 \
     -out /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APD_synth

# python run_experiment_apd_all.py \
#     -in /datagrid/Medical/microscopy/drosophila/synthetic_data/atomicPatternDictionary3D_v0 \
#     -out /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APD

# python run_experiment_apd_all.py \
#     -in /datagrid/Medical/microscopy/drosophila/synthetic_data/atomicPatternDictionary3D_v1 \
#     -out /datagrid/Medical/microscopy/drosophila/TEMPORARY/experiments_APD \
#     --nb_jobs 1